This is project use Express + Typecript + Webpack to build API Security

# Available Scripts

In the project directory, you can run: 

### npm run webpack

Build the app for development to the dist folder 


### npm start

Runs the app in the development mode. Open [http://localhost:8080](http://localhost:8080/) to view it in the browser
The page will reload if you makes edits. You will also see any lint errors in the console
